#change location
DATE="$1"
MC="$3" #ttbar or Zprime
TASK="$2"

mkdir /afs/cern.ch/atlas/groups/validation/Btagging/Rel22_${DATE}/
mkdir /afs/cern.ch/atlas/groups/validation/Btagging/Rel22_${DATE}/Task${TASK}_${MC}
cp -r plots/ /afs/cern.ch/atlas/groups/validation/Btagging/Rel22_${DATE}/Task${TASK}_${MC}
cp -r ROC/ /afs/cern.ch/atlas/groups/validation/Btagging/Rel22_${DATE}/Task${TASK}_${MC}
cp -r ROC_VR/ /afs/cern.ch/atlas/groups/validation/Btagging/Rel22_${DATE}/Task${TASK}_${MC}
cp -r ROC_PFlow/ /afs/cern.ch/atlas/groups/validation/Btagging/Rel22_${DATE}/Task${TASK}_${MC}
