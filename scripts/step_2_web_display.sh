setupATLAS
asetup 22.0.32,Athena
MERGED_REFFILE="files_merged/merged_NTUP_PHYSVAL_ref.root"
MERGED_TESTFILE="files_merged/merged_NTUP_PHYSVAL_test.root"
REF_NAME="ref" #(this sets the legend of the plot, choose a fitting name here)
TEST_NAME="test" #(legend for test)
physval_make_web_display.py --reffile $REF_NAME:$MERGED_REFFILE --outdir=plots $MERGED_TESTFILE --title=$TEST_NAME --logy --normalize --ratio --startpath="BTag"
#run the script to add line breaks to the html output (ensure the first line points to the plots directory)
bash add_line_breaks_to_html.sh
