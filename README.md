# Physics Validation Scripts

Fairly simple to run. You may need to adjust the location for copying files in the setup scripts. Only works out of the box for ttbar and Zprime samples.


The `/scripts` directory should be placed as such:
```
.../validation/scripts/
```

I work in a directory such as:
```
.../validation/Rel22_X-X-X/TaskA_ttbar/
```

Here, X-X-X is the date. In this directory for a ttbar task I then run:
```
source ../../setup_dir_ttbar.sh
```

This will create / copy all the directories and scripts you need to run the task.

From here you must rucio the task reference and test root files into the `ref` and `test` directories.

Last but not least, you just run the `run_steer.sh` script with its corresponding arguments, such as:
```
source run_steer.sh X-X-X A ttbar
``` 

This shell script does the rest! Even copies the histograms into the /eos website location. 

Visit http://atlas-computing.web.cern.ch/atlas-computing/links/PhysValDir/Btagging/ 

From here you'll need to make the slides using the ftaghelper script.


