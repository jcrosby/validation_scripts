# then run the script
echo "Running PFlow ROC"
root -l -b -q Draw_PhysVal_btagROC_PFlow.c
echo "Running VR ROC"
root -l -b -q Draw_PhysVal_btagROC_VR.c
# and run the script to create the html page
python CreatePhysValWebPage.py -i ROC/
python CreatePhysValWebPage.py -i ROC_VR/
python CreatePhysValWebPage.py -i ROC_PFlow/
